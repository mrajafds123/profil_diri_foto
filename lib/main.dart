import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter UI Controls',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Profil Diri'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  String _dropdownButtonValue = 'One';
  String _popupMenuButtonValue = 'One';
  bool _checkboxValue1 = false;
  bool _checkboxValue2 = false;
  bool _checkboxValue3 = false;
  bool _checkboxValue4 = false;
  String _radioBoxValue = 'One';
  double _sliderValue = 10;
  bool _switchValue = false;
  String gender = 'Laki-Laki';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.yellow.shade100,
        key: _scaffoldKey,
        appBar: AppBar(
          title: Text(widget.title),
        ),
        body: Container(
            padding: EdgeInsets.only(left: 10, right: 10),
            child: SingleChildScrollView(
              child: Column(mainAxisAlignment: MainAxisAlignment.start, crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
                // Buttons

                Column(mainAxisAlignment: MainAxisAlignment.start, crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
                  SizedBox(
                    height: 5.0,
                  ),
                  //Column(
                  //children: <Widget>[
                  //Text('Nama :Mohd.Raja Firdaus\nNim :1990343028\nKelas :TRKJ3A\nAplikasi Becak Online'),
                  //  ],
                  // ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      SizedBox(
                        height: 20.0,
                      ),
                      TextField(
                        obscureText: false,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.blueGrey.shade50,
                          border: OutlineInputBorder(),
                          icon: Text("Nama  "),
                          hintText: "Nama Lengkap",
                          labelText: 'Nama',
                        ),
                      ),
                    ],
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      SizedBox(
                        height: 20.0,
                      ),
                      TextField(
                        obscureText: false,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.blueGrey.shade50,
                          border: OutlineInputBorder(),
                          icon: Text("Alamat"),
                          hintText: "Alamat Rumah",
                          labelText: 'Alamat',
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 15.0,
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Column(
                        children: <Widget>[
                          Text('Jenis Kelamin:', style: TextStyle(fontSize: 17, fontWeight: FontWeight.bold)),
                        ],
                      ),
                      Column(children: [
                        Row(children: [
                          SizedBox(
                            width: 20.0,
                            child: Radio(
                              value: 'Laki-Laki',
                              groupValue: gender,
                              activeColor: Colors.blue,
                              onChanged: (value) {
                                //value may be true or false
                                setState(() {
                                  gender = value;
                                });
                              },
                            ),
                          ),
                          SizedBox(width: 10.0),
                          Text('Laki-Laki')
                        ]),
                        Row(children: [
                          SizedBox(
                            width: 20.0,
                            child: Radio(
                              value: 'Perempuan',
                              groupValue: gender,
                              activeColor: Colors.blue,
                              onChanged: (value) {
                                //value may be true or false
                                setState(() {
                                  gender = value;
                                });
                              },
                            ),
                          ),
                          SizedBox(width: 8.0),
                          Text('Perempuan')
                        ]),
                        SizedBox(
                          height: 15.0,
                        ),
                        Column(mainAxisAlignment: MainAxisAlignment.start, crossAxisAlignment: CrossAxisAlignment.center, children: <Widget>[
                          Column(
                            children: <Widget>[
                              Text('Foto:', style: TextStyle(fontSize: 17, fontWeight: FontWeight.bold)),
                            ],
                          ),
                          Image.asset(
                            'assets/images/daus.png',
                            height: 250,
                            width: 200,
                          ),
                          Column(mainAxisAlignment: MainAxisAlignment.start, crossAxisAlignment: CrossAxisAlignment.center, children: <Widget>[
                            Column(
                              children: <Widget>[
                                Text('Hobi:', style: TextStyle(fontSize: 17, fontWeight: FontWeight.bold)),
                              ],
                            ),
                            CheckboxListTile(
                              title: Text('Buat Film Pendek'),
                              value: _checkboxValue1,
                              activeColor: Colors.blue,
                              onChanged: (value) {
                                setState(() {
                                  _checkboxValue1 = value;
                                });
                              },
                            ),
                            CheckboxListTile(
                              title: Text('Badminton'),
                              value: _checkboxValue2,
                              activeColor: Colors.blue,
                              onChanged: (value) {
                                setState(() {
                                  _checkboxValue2 = value;
                                });
                              },
                            ),
                            CheckboxListTile(
                              title: Text('BeatBox'),
                              value: _checkboxValue3,
                              activeColor: Colors.blue,
                              onChanged: (value) {
                                setState(() {
                                  _checkboxValue3 = value;
                                });
                              },
                            ),
                            SizedBox(
                              height: 15.0,
                            ),
                            Column(mainAxisAlignment: MainAxisAlignment.start, crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
                              Row(
                                children: <Widget>[
                                  RaisedButton(
                                    child: Text(
                                      "Submit",
                                      style: TextStyle(color: Colors.black),
                                    ),
                                    color: Colors.blue,
                                    onPressed: () {},
                                  ),
                                  Text(
                                    "    ",
                                  ),
                                  RaisedButton(
                                    child: Text(
                                      "Cancel",
                                      style: TextStyle(color: Colors.black),
                                    ),
                                    color: Colors.blue,
                                    onPressed: () {},
                                  ),
                                ],
                              )
                            ]),
                            SizedBox(
                              height: 15.0,
                            ),
                          ]),
                        ]),
                      ]),
                    ],
                  ),
                ])
              ]),
            )));
  }
}
